#ifndef ROCKET_H
#define ROCKET_H

#include "Stage.h"

/**
 * @brief The Rocket class
 */
class Rocket
{
public:
    Rocket();
    ~Rocket();

    void addStage(const Stage& stage);
    void removeStage();
    void clear();

    void replaceStage(int i, const Stage& stage);

    const Stage* getStage(int i) const;
    int getStagePosition(const Stage* stage) const;

    void setStages(const QVector<Stage>& stages);
    const QVector<Stage*>& getAllStage() const;

    int size() const;
    float deltaV() const;

protected:
    void calculateDeltaV();

private:
    QVector<Stage*> mStages;
    float mDeltaV;
};

QDataStream& operator<<(QDataStream& out, const Rocket& rocket);
QDataStream& operator>>(QDataStream& in, Rocket& rocket);

#endif // ROCKET_H
