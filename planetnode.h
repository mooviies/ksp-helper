#ifndef PLANETNODE_H
#define PLANETNODE_H

#include <QString>
#include <QPixmap>
#include <QVector>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>

/**
 * @brief The PlanetNode class
 */
class PlanetNode
{
public:
    enum NodeType
    {
        Body,
        LowOrbit,
        Intercept
    };

    PlanetNode(QString name, QString imagePath, QPointF imagePos, QGraphicsScene& scene, NodeType type, bool atmosphere = false);
    QGraphicsPixmapItem* getItem();
    const QGraphicsPixmapItem* getItem() const;

    const QString& name() const { return mName; }
    float deltaV() const { return mParentDistance; }
    bool atmosphere() const { return mAtmosphere; }
    NodeType getType() const { return mType; }

private:
    friend class PlanetTree;

    PlanetNode* mParent;
    float mParentDistance;
    bool mAtmosphere;

    NodeType mType;
    QString mName;
    QVector<PlanetNode*> mChilds;
    QVector<float> mChildsDistance;
    QGraphicsPixmapItem* mItem;
};

QDataStream& operator<<(QDataStream& out, PlanetNode::NodeType& nodeType);
QDataStream& operator>>(QDataStream& in, PlanetNode::NodeType& nodeType);

#endif // PLANETNODE_H
