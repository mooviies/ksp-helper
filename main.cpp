#include <QApplication>

#include "MainWindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    if(argc > 1)
    {
        w.setFilePath(QString(argv[1]));
        w.load();
    }

    w.show();

    return a.exec();
}
