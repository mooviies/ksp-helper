#ifndef PLANETTREE_H
#define PLANETTREE_H

#include <QSet>

#include "PlanetNode.h"

struct Course_V100
{
    QVector<QString> names;
    QHash<QString, float> deltaV;

    float totalDeltaV = 0;
};

struct Course
{
    QVector<QString> names;
    QHash<QString, PlanetNode::NodeType> types;
    QHash<QString, float> deltaV;

    QVector<QString> stageNames;
    QHash<QString, float> stagesDeltaV;

    float totalStagesDeltaV = 0;
    float totalDeltaV = 0;
};

QDataStream& operator<<(QDataStream& out, const Course_V100& course);
QDataStream& operator>>(QDataStream& in, Course_V100& course);

QDataStream& operator<<(QDataStream& out, const Course& course);
QDataStream& operator>>(QDataStream& in, Course& course);

struct NodePath
{
    QVector<const PlanetNode*> nodes;
    float totalDistance = 0;
};

/**
 * @brief The PlanetTree class
 */
class PlanetTree
{
public:
    PlanetTree(PlanetNode* root = 0);
    ~PlanetTree();

    PlanetNode* setRoot(PlanetNode* root);
    PlanetNode* addNode(PlanetNode* parent, PlanetNode* node, float distance);
    const PlanetNode* getNode(QGraphicsItem* item) const;
    const PlanetNode* getNode(const QString& name) const;

    NodePath getPath(const PlanetNode* destination) const;

protected:
    void getRecursiveCourse(const PlanetNode* currentNode, NodePath& course) const;

private:
    PlanetNode* mRoot;
    QHash<QGraphicsItem*, PlanetNode*> mAllNode;
    QHash<QString, PlanetNode*> mNodeByName;
};

#endif // PLANETTREE_H
