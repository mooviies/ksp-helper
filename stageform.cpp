#include "StageForm.h"
#include "ui_StageForm.h"

#include <QtScript/QScriptEngine>
#include <QShortcut>

StageForm::StageForm(const Rocket& rocket, const Course& course, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::StageForm)
    , mRocket(rocket)
    , mCourse(course)
    , mRecommended(0)
{
    ui->setupUi(this);
    ui->lineEditName->setText("Stage " + QVariant(mRocket.size()).toString());
    mLineEditPalette = ui->lineEditTotalMass->palette();

    mStagePos = mRocket.size();
    if(mStagePos > 0)
    {
        mStage.setNext(mRocket.getStage(mStagePos - 1));
        displayNextStage();
    }

    configureShortcut();
    calculateRecommendedV();
}

StageForm::StageForm(const Rocket& rocket, const Course& course, const Stage* stage, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::StageForm)
    , mRocket(rocket)
    , mCourse(course)
{
    ui->setupUi(this);

    mStage = *stage;
    mStagePos = mRocket.getStagePosition(stage);
    ui->lineEditName->setText(mStage.name());

    if(mStage.next())
        displayNextStage();

    configureShortcut();
    calculateRecommendedV();

    processParts();
    processFuel();
    processTrust();
    processStageValues();
}

StageForm::~StageForm()
{
    delete ui;
}

const Stage& StageForm::getStage() const
{
    return mStage;
}

void StageForm::processParts()
{
    ui->listWidgetPart->clear();
    const MassList& massList = mStage.getParts();

    foreach(Mass mass, massList)
    {
        ui->listWidgetPart->addItem(QVariant(mass.value()).toString());
    }
    processStageValues();
}

void StageForm::processFuel()
{
    ui->listWidgetDry->clear();
    ui->listWidgetFuel->clear();

    const FuelList& fuelList = mStage.getFuel();

    foreach(Fuel fuel, fuelList)
    {
        ui->listWidgetDry->addItem(QVariant(fuel.dryMass()).toString());
        ui->listWidgetFuel->addItem(QVariant(fuel.fuelMass()).toString());
    }
    processStageValues();
}

void StageForm::processTrust()
{
    ui->listWidgetISP->clear();
    ui->listWidgetTrust->clear();

    const TrustList& trustList = mStage.getTrust();

    foreach(Trust trust, trustList)
    {
        ui->listWidgetISP->addItem(QVariant(trust.isp()).toString());
        ui->listWidgetTrust->addItem(QVariant(trust.trustValue()).toString());
    }
    processStageValues();
}

void StageForm::processStageValues()
{
    ui->lineEditTotalMass->setText(QVariant(mStage.mass()).toString());
    ui->lineEditTotalDryMass->setText(QVariant(mStage.dryMass()).toString());
    ui->lineEditTotalISP->setText(QVariant(mStage.getTrustTotal().isp()).toString());
    ui->lineEditTotalTrust->setText(QVariant(mStage.getTrustTotal().trustValue()).toString());
    ui->lineEditTotalDeltaV->setText(QVariant(mStage.deltaV()).toString());

    setColorState(ui->lineEditTotalTrust,
                  mStage.getTrustTotal().trustValue(),
                  mStage.mass() * 10,
                  mStage.mass() * 10 / MINIMAL_TRUST_RATIO,
                  "The trust can't lift the rocket",
                  "The trust is barely enough to lift the rocket");

    setColorState(ui->lineEditTotalDeltaV,
                  mStage.deltaV(),
                  mRecommended,
                  mRecommended,
                  "You need more fuel or bigger ISP");
}

void StageForm::calculateFuel()
{
    if(ui->lineEditContainerFuel->text().isEmpty())
    {
        if(ui->checkBoxAutomatic->isChecked())
            ui->lineEditFuel->setText("0");
        return;
    }

    QScriptEngine engine;
    Fuel fuel(engine.evaluate(ui->lineEditContainerFuel->text()).toNumber());
    ui->lineEditFuel->setText(QVariant(fuel.fuelMass()).toString());
}

void StageForm::displayNextStage()
{
    ui->lineEditNextName->setText(mStage.next()->name());
    ui->lineEditNextMass->setText(QVariant(mStage.next()->mass()).toString());
}

void StageForm::calculateRecommendedV()
{
    if(mCourse.names.size() > 0)
    {
        if(mStagePos < mCourse.stageNames.size())
        {
            QString stageName = mCourse.stageNames.at(mStagePos);
            displayRecommendedV(stageName, mCourse.stagesDeltaV.value(stageName));
        }
    }
}

void StageForm::displayRecommendedV(const QString& name, float recommended)
{
    mRecommended = recommended;
    int recommendedInt = recommended;
    ui->lineEditName->setText(name);

    QString deltaVString = QVariant(recommendedInt).toString();

    if(recommendedInt == 0)
        deltaVString = "Aerobrake";

    ui->lineEditRecommendation->setText(deltaVString);
}

void StageForm::configureShortcut()
{
    QShortcut* deleteItemSC = new QShortcut(QKeySequence(Qt::Key_Delete), this);
    connect(deleteItemSC, SIGNAL(activated()), this, SLOT(deleteItem()));
}

void StageForm::on_buttonAddPart_clicked()
{
    QScriptEngine engine;
    mStage.addPart(engine.evaluate(ui->lineEditPart->text()).toNumber());

    ui->lineEditPart->clear();
    ui->lineEditPart->setFocus();

    processParts();
}

void StageForm::on_buttonAddFuel_clicked()
{
    QScriptEngine engine;

    float container = engine.evaluate(ui->lineEditContainerFuel->text()).toNumber();
    float fuel = engine.evaluate(ui->lineEditFuel->text()).toNumber();
    mStage.addFuel(Fuel(container - fuel, fuel));

    ui->lineEditContainerFuel->clear();
    ui->lineEditFuel->clear();
    ui->lineEditContainerFuel->setFocus();

    processFuel();
}

void StageForm::on_buttonAddTrust_clicked()
{
    QScriptEngine engine;

    float isp = engine.evaluate(ui->lineEditISP->text()).toNumber();
    float trust = engine.evaluate(ui->lineEditTrust->text()).toNumber();

    mStage.addTrust(Trust(isp, trust));

    ui->lineEditISP->clear();
    ui->lineEditTrust->clear();

    ui->lineEditTrust->setFocus();

    processTrust();
}

void StageForm::deleteItem()
{
    if(ui->listWidgetPart->hasFocus())
        deleteItemPart();
    else if(ui->listWidgetDry->hasFocus())
        deleteItemFuel();
    else if(ui->listWidgetFuel->hasFocus())
        deleteItemFuel();
    else if(ui->listWidgetISP->hasFocus())
        deleteItemTrust();
    else if(ui->listWidgetTrust->hasFocus())
        deleteItemTrust();
}

void StageForm::deleteItemPart()
{
    int offset = 0;
    foreach(QListWidgetItem* item, ui->listWidgetPart->selectedItems())
    {
        mStage.removePart(ui->listWidgetPart->row(item) - offset);
        offset++;
    }
    processParts();
}

void StageForm::deleteItemFuel()
{
    QListWidget* listWidget = 0;

    if(ui->listWidgetDry->hasFocus())
        listWidget = ui->listWidgetDry;
    else
        listWidget = ui->listWidgetFuel;

    int offset = 0;
    foreach(QListWidgetItem* item, listWidget->selectedItems())
    {
        mStage.removeFuel(listWidget->row(item) - offset);
        offset++;
    }
    processFuel();
}

void StageForm::deleteItemTrust()
{
    QListWidget* listWidget = 0;

    if(ui->listWidgetISP->hasFocus())
        listWidget = ui->listWidgetISP;
    else
        listWidget = ui->listWidgetTrust;

    int offset = 0;
    foreach(QListWidgetItem* item, listWidget->selectedItems())
    {
        mStage.removeTrust(listWidget->row(item) - offset);
        offset++;
    }
    processTrust();
}

void StageForm::on_lineEditPart_textChanged(const QString &arg1)
{
    ui->buttonAddPart->setEnabled(!arg1.isEmpty());
}

void StageForm::on_lineEditContainerFuel_textChanged(const QString &arg1)
{
    if(ui->checkBoxAutomatic->isChecked())
        calculateFuel();

    ui->buttonAddFuel->setEnabled(!arg1.isEmpty());
}

void StageForm::on_lineEditFuel_textChanged(const QString &arg1)
{
    ui->buttonAddFuel->setEnabled(!arg1.isEmpty());
}

void StageForm::on_lineEditISP_textChanged(const QString &arg1)
{
    ui->buttonAddTrust->setEnabled(!arg1.isEmpty());
}

void StageForm::on_lineEditTrust_textChanged(const QString &arg1)
{
    ui->buttonAddTrust->setEnabled(!arg1.isEmpty());
}

void StageForm::on_checkBoxAutomatic_toggled(bool checked)
{
    if(checked)
        calculateFuel();
}

void StageForm::on_listWidgetDry_itemSelectionChanged()
{
    if(ui->listWidgetDry->hasFocus())
    {
        if(ui->listWidgetDry->count() != ui->listWidgetFuel->count())
            return;

        for(int i = 0; i < ui->listWidgetDry->count(); i++)
        {
            bool selected = ui->listWidgetDry->item(i)->isSelected();
            ui->listWidgetFuel->item(i)->setSelected(selected);
        }
    }
}

void StageForm::on_listWidgetFuel_itemSelectionChanged()
{
    if(ui->listWidgetFuel->hasFocus())
    {
        if(ui->listWidgetDry->count() != ui->listWidgetFuel->count())
            return;

        for(int i = 0; i < ui->listWidgetFuel->count(); i++)
        {
            bool selected = ui->listWidgetFuel->item(i)->isSelected();
            ui->listWidgetDry->item(i)->setSelected(selected);
        }
    }
}

void StageForm::on_listWidgetISP_itemSelectionChanged()
{
    if(ui->listWidgetISP->hasFocus())
    {
        if(ui->listWidgetISP->count() != ui->listWidgetTrust->count())
            return;

        for(int i = 0; i < ui->listWidgetISP->count(); i++)
        {
            bool selected = ui->listWidgetISP->item(i)->isSelected();
            ui->listWidgetTrust->item(i)->setSelected(selected);
        }
    }
}

void StageForm::on_listWidgetTrust_itemSelectionChanged()
{
    if(ui->listWidgetTrust->hasFocus())
    {
        if(ui->listWidgetISP->count() != ui->listWidgetTrust->count())
            return;

        for(int i = 0; i < ui->listWidgetTrust->count(); i++)
        {
            bool selected = ui->listWidgetTrust->item(i)->isSelected();
            ui->listWidgetISP->item(i)->setSelected(selected);
        }
    }
}

void StageForm::on_lineEditName_textChanged(const QString &arg1)
{
    mStage.setName(arg1);
}

void setColorState(QWidget* widget, float currentValue, float yellowValue, float greenValue,
                              const QString& redTooltip, const QString& yellowTooltip)
{
    QPalette statePalette = widget->palette();

    if(currentValue < yellowValue)
    {
        statePalette.setColor(QPalette::Base, QColor(255, 204, 204));
        widget->setToolTip(redTooltip);
    }
    else if(currentValue < greenValue)
    {
        statePalette.setColor(QPalette::Base, QColor(255, 255, 204));
        widget->setToolTip(yellowTooltip);
    }
    else
    {
        statePalette.setColor(QPalette::Base, QColor(204, 255, 204));
        widget->setToolTip("");
    }

    widget->setPalette(statePalette);
}
