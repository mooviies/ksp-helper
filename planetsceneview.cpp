#include "PlanetSceneView.h"

#include <QMouseEvent>
#include <QGraphicsItem>

PlanetSceneView::PlanetSceneView(QWidget *parent)
    : QGraphicsView(parent)
    , mLastSelectedItem(0)
    , mBackground(0)
    , mPlanetTree(0)
{
    mScale = 1.5;
    setMouseTracking(true);
}

void PlanetSceneView::setBackground(QGraphicsItem* background)
{
    mBackground = background;
}

void PlanetSceneView::setPlanetTree(PlanetTree* planetTree)
{
    mPlanetTree = planetTree;
}

void PlanetSceneView::mouseMoveEvent(QMouseEvent* event)
{
    QGraphicsItem* selectedItem = itemAt(event->pos());

    if(mLastSelectedItem != selectedItem)
    {
        if(mLastSelectedItem)
        {
            foreach(const PlanetNode* nodec, mLastCourse.nodes)
            {
                PlanetNode* node = (PlanetNode*) nodec;
                node->getItem()->setScale(1);
                node->getItem()->setZValue(0);
            }

            mLastCourse = mSavedCourse;
        }

        if(selectedItem != mBackground)
        {
            mLastSelectedItem = selectedItem;
            mLastCourse = mPlanetTree->getPath(mPlanetTree->getNode(mLastSelectedItem));

            foreach(const PlanetNode* nodec, mLastCourse.nodes)
            {
                PlanetNode* node = (PlanetNode*) nodec;
                node->getItem()->setScale(mScale);
                node->getItem()->setZValue(1);
            }
        }
        else
            mLastSelectedItem = 0;

        emit courseChanged(mLastCourse);
    }
}

void PlanetSceneView::mouseReleaseEvent(QMouseEvent* /*event*/)
{
    if(!mLastCourse.nodes.empty())
    {
        mSavedCourse = mLastCourse;
        emit courseSelected();
    }
    else
    {
        mSavedCourse.nodes.clear();
        mSavedCourse.totalDistance = 0;
    }
}
