#include "DestinationChooser.h"
#include "ui_DestinationChooser.h"

#include <QGraphicsPixmapItem>

#include <cmath>

DestinationChooser::DestinationChooser(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::DestinationChooser)
    , mBackground(":/images/kerbinDeltaVMap.png")
    , mDres(":/images/dres.png")
    , mDuna(":/images/duna.png")
    , mEeloo(":/images/eeloo.png")
    , mEve(":/images/eve.png")
    , mJool(":/images/jool.png")
    , mKerbin(":/images/kerbin.png")
    , mMoho(":/images/moho.png")
    , mLaythe(":/images/laythe.png")
    , mKerbinBodies(":/images/kerbinBodies.png")
    , mDunaLowOrbit(":/images/dunaLowOrbit.png")
    , mEveLowOrbit(":/images/eveLowOrbit.png")
    , mJoolLowOrbit(":/images/joolLowOrbit.png")
    , mKerbinLowOrbit(":/images/kerbinLowOrbit.png")
    , mKerbinIntercept(":/images/kerbinIntercept.png")
{
    ui->setupUi(this);

    mBackgroundItem = mScene.addPixmap(mBackground);
    mBackgroundItem->setTransformationMode(Qt::SmoothTransformation);
    mBackgroundItem->setTransformOriginPoint(mBackgroundItem->boundingRect().center());

    ui->graphicsViewDeltaV->setBackground(mBackgroundItem);
    ui->graphicsViewDeltaV->setPlanetTree(&mPlanetTree);
    ui->graphicsViewDeltaV->setScene(&mScene);
    mScene.setSceneRect(mBackgroundItem->pixmap().rect());

    connect(ui->graphicsViewDeltaV, SIGNAL(courseChanged(const NodePath&)), this, SLOT(setCourse(const NodePath&)));
    connect(ui->graphicsViewDeltaV, SIGNAL(courseSelected()), this, SLOT(accept()));

    PlanetNode* kerbin = mPlanetTree.setRoot(new PlanetNode("Kerbin", mKerbin, QPointF(434, 960), mScene, PlanetNode::Body, true));
    PlanetNode* kerbinLO = mPlanetTree.addNode(kerbin, new PlanetNode("Kerbin Low Orbit", mKerbinLowOrbit, QPointF(434, 828), mScene, PlanetNode::LowOrbit), 4550);
    mPlanetTree.addNode(kerbinLO, new PlanetNode("Kerbin GSO", mKerbinBodies, QPointF(335, 828), mScene, PlanetNode::LowOrbit), 800);
    PlanetNode* kerbinIntercept = mPlanetTree.addNode(kerbinLO, new PlanetNode("Kerbin Intercept", mKerbinIntercept, QPointF(425, 658), mScene, PlanetNode::Intercept), 950);

        PlanetNode* munIntercept = mPlanetTree.addNode(kerbinLO, new PlanetNode("Mün Intercept", mKerbinBodies, QPointF(362, 743), mScene, PlanetNode::Intercept), 860);
        PlanetNode* munLO = mPlanetTree.addNode(munIntercept, new PlanetNode("Mün Low Orbit", mKerbinBodies, QPointF(277, 743), mScene, PlanetNode::LowOrbit), 210);
        mPlanetTree.addNode(munLO, new PlanetNode("Mün", mKerbinBodies, QPointF(197, 743), mScene, PlanetNode::Body), 640);

        PlanetNode* minmusIntercept = mPlanetTree.addNode(kerbinLO, new PlanetNode("Minmus Intercept", mKerbinBodies, QPointF(523, 719), mScene, PlanetNode::Intercept), 920);
        PlanetNode* minmusLO = mPlanetTree.addNode(minmusIntercept, new PlanetNode("Minmus Low Orbit", mKerbinBodies, QPointF(604, 719), mScene, PlanetNode::LowOrbit), 80);
        mPlanetTree.addNode(minmusLO, new PlanetNode("Minmus", mKerbinBodies, QPointF(706, 718), mScene, PlanetNode::Body), 240);

    PlanetNode* dresIntercept = mPlanetTree.addNode(kerbinIntercept, new PlanetNode("Dres Intercept", mDres, QPointF(330, 111), mScene, PlanetNode::Intercept), 350);
    PlanetNode* dresLO = mPlanetTree.addNode(dresIntercept, new PlanetNode("Dres Low Orbit", mDres, QPointF(213, 111), mScene, PlanetNode::LowOrbit), 800);
    mPlanetTree.addNode(dresLO, new PlanetNode("Dres", mDres, QPointF(95, 111), mScene, PlanetNode::Body), 555);

    PlanetNode* mohoIntercept = mPlanetTree.addNode(kerbinIntercept, new PlanetNode("Moho Intercept", mMoho, QPointF(281, 658), mScene, PlanetNode::Intercept), 730);
    PlanetNode* mohoLO = mPlanetTree.addNode(mohoIntercept, new PlanetNode("Moho Low Orbit", mMoho, QPointF(178, 658), mScene, PlanetNode::LowOrbit), 2200);
    mPlanetTree.addNode(mohoLO, new PlanetNode("Moho", mMoho, QPointF(66, 658), mScene, PlanetNode::Body), 1400);

    PlanetNode* eelooIntercept = mPlanetTree.addNode(kerbinIntercept, new PlanetNode("Eeloo Intercept", mEeloo, QPointF(585, 658), mScene, PlanetNode::Intercept), 1150);
    PlanetNode* eelooLO = mPlanetTree.addNode(eelooIntercept, new PlanetNode("Eeloo Low Orbit", mEeloo, QPointF(714, 658), mScene, PlanetNode::LowOrbit), 2100);
    mPlanetTree.addNode(eelooLO, new PlanetNode("Eeloo", mEeloo, QPointF(859, 658), mScene, PlanetNode::Body), 840);

    PlanetNode* eveIntercept = mPlanetTree.addNode(kerbinIntercept, new PlanetNode("Eve Intercept", mEve, QPointF(338, 510), mScene, PlanetNode::Intercept), 80);
    PlanetNode* eveLO = mPlanetTree.addNode(eveIntercept, new PlanetNode("Eve Low Orbit", mEveLowOrbit, QPointF(235, 500), mScene, PlanetNode::LowOrbit), 1310);
    mPlanetTree.addNode(eveLO, new PlanetNode("Eve", mEve, QPointF(61, 511), mScene, PlanetNode::Body, true), 12000);

        PlanetNode* gillyIntercept = mPlanetTree.addNode(eveLO, new PlanetNode("Gilly Intercept", mEve, QPointF(183, 438), mScene, PlanetNode::Intercept), 1650);
        PlanetNode* gillyLO = mPlanetTree.addNode(gillyIntercept, new PlanetNode("Gilly Low Orbit", mEve, QPointF(122, 438), mScene, PlanetNode::LowOrbit), 210);
        mPlanetTree.addNode(gillyLO, new PlanetNode("Gilly", mEve, QPointF(62, 438), mScene, PlanetNode::Body), 35);

    PlanetNode* dunaIntercept = mPlanetTree.addNode(kerbinIntercept, new PlanetNode("Duna Intercept", mDuna, QPointF(333, 310), mScene, PlanetNode::Intercept), 110);
    PlanetNode* dunaLO = mPlanetTree.addNode(dunaIntercept, new PlanetNode("Duna Low Orbit", mDunaLowOrbit, QPointF(235, 300), mScene, PlanetNode::LowOrbit), 370);
    mPlanetTree.addNode(dunaLO, new PlanetNode("Duna", mDuna, QPointF(62, 311), mScene, PlanetNode::Body, true), 1380);

        PlanetNode* ikeIntercept = mPlanetTree.addNode(dunaLO, new PlanetNode("Ike Intercept", mDuna, QPointF(184, 238), mScene, PlanetNode::Intercept), 270);
        PlanetNode* ikeLO = mPlanetTree.addNode(ikeIntercept, new PlanetNode("Ike Low Orbit", mDuna, QPointF(124, 238), mScene, PlanetNode::LowOrbit), 110);
        mPlanetTree.addNode(ikeLO, new PlanetNode("Ike", mDuna, QPointF(62, 238), mScene, PlanetNode::Body), 535);

    PlanetNode* joolIntercept = mPlanetTree.addNode(kerbinIntercept, new PlanetNode("Jool Intercept", mJool, QPointF(454, 381), mScene, PlanetNode::Intercept), 965);
    PlanetNode* joolLO = mPlanetTree.addNode(joolIntercept, new PlanetNode("Jool Low Orbit", mJoolLowOrbit, QPointF(573, 303), mScene, PlanetNode::LowOrbit), 2630);

        PlanetNode* polIntercept = mPlanetTree.addNode(joolLO, new PlanetNode("Pol Intercept", mJool, QPointF(573, 174), mScene, PlanetNode::Intercept), 2500);
        PlanetNode* polLO = mPlanetTree.addNode(polIntercept, new PlanetNode("Pol Low Orbit", mJool, QPointF(573, 95), mScene, PlanetNode::LowOrbit), 900);
        mPlanetTree.addNode(polLO, new PlanetNode("Pol", mJool, QPointF(573, 25), mScene, PlanetNode::Body), 180);

        PlanetNode* bopIntercept = mPlanetTree.addNode(joolLO, new PlanetNode("Bop Intercept", mJool, QPointF(721, 182), mScene, PlanetNode::Intercept), 2430);
        PlanetNode* bopLO = mPlanetTree.addNode(bopIntercept, new PlanetNode("Bop Low Orbit", mJool, QPointF(721, 104), mScene, PlanetNode::LowOrbit), 950);
        mPlanetTree.addNode(bopLO, new PlanetNode("Bop", mJool, QPointF(721, 32), mScene, PlanetNode::Body), 276);

        PlanetNode* tyloIntercept = mPlanetTree.addNode(joolLO, new PlanetNode("Tylo Intercept", mJool, QPointF(681, 293), mScene, PlanetNode::Intercept), 2190);
        PlanetNode* tyloLO = mPlanetTree.addNode(tyloIntercept, new PlanetNode("Tylo Low Orbit", mJool, QPointF(777, 292), mScene, PlanetNode::LowOrbit), 940);
        mPlanetTree.addNode(tyloLO, new PlanetNode("Tylo", mJool, QPointF(923, 292), mScene, PlanetNode::Body), 3070);

        PlanetNode* vallIntercept = mPlanetTree.addNode(joolLO, new PlanetNode("Vall Intercept", mJool, QPointF(682, 316), mScene, PlanetNode::Intercept), 1970);
        PlanetNode* vallLO = mPlanetTree.addNode(vallIntercept, new PlanetNode("Vall Low Orbit", mJool, QPointF(778, 316), mScene, PlanetNode::LowOrbit), 790);
        mPlanetTree.addNode(vallLO, new PlanetNode("Vall", mJool, QPointF(925, 316), mScene, PlanetNode::Body), 1180);

        PlanetNode* laytheIntercept = mPlanetTree.addNode(joolLO, new PlanetNode("Laythe Intercept", mJool, QPointF(732, 426), mScene, PlanetNode::Intercept), 1600);
        PlanetNode* laytheLO = mPlanetTree.addNode(laytheIntercept, new PlanetNode("Laythe Low Orbit", mJool, QPointF(800, 494), mScene, PlanetNode::LowOrbit), 780);
        mPlanetTree.addNode(laytheLO, new PlanetNode("Laythe", mLaythe, QPointF(883, 574), mScene, PlanetNode::Body, true), 2800);
}

DestinationChooser::~DestinationChooser()
{
    delete ui;
}

const Course& DestinationChooser::getCourse() const
{
    return mCourse;
}

const Course DestinationChooser::getCourse(const Course_V100& course_V100) const
{
    Course course;

    if(!course_V100.names.empty())
    {
        foreach(QString name, course_V100.names)
        {
            if(!name.endsWith("-R"))
            {
                name.truncate(name.indexOf(" "));
                const PlanetNode* node = mPlanetTree.getNode(name);
                course = createCourse(mPlanetTree.getPath(node));
                break;
            }
        }
    }

    return course;
}

void DestinationChooser::showEvent(QShowEvent*event)
{
    ui->graphicsViewDeltaV->fitInView(mBackgroundItem, Qt::KeepAspectRatio);
    QWidget::showEvent(event);
}

void DestinationChooser::resizeEvent(QResizeEvent* event)
{
    ui->graphicsViewDeltaV->fitInView(mBackgroundItem, Qt::KeepAspectRatio);
    QWidget::resizeEvent(event);
}

Course DestinationChooser::createCourse(const NodePath& nodePath) const
{
    Course course;
    course.deltaV.clear();
    course.names.clear();
    course.stageNames.clear();
    course.stagesDeltaV.clear();
    course.totalDeltaV = 0;
    course.totalStagesDeltaV = 0;
    course.types.clear();

    ui->lineEditDVMin->setText("0");
    ui->lineEditDVSafe->setText("0");
    ui->lineEditNbStages->setText("0");

    ui->listWidgetDeltaV->clear();
    for(int i = 0; i < nodePath.nodes.size(); i++)
    {
        const PlanetNode* node = nodePath.nodes.at(i);

        float deltaV = node->deltaV();

        if(node->atmosphere())
            deltaV = 0;

        course.names.append(node->name());
        course.types[node->name()] = nodePath.nodes.at(i)->getType();
        course.deltaV[node->name()] = deltaV;
        course.totalDeltaV += deltaV;

        if(node->deltaV() > 0)
        {
            ui->listWidgetDeltaV->insertItem(0, node->name() + " : " + QVariant(deltaV).toString());

            if(!ui->checkBoxOneWay->isChecked())
            {
                const PlanetNode* nextNode = nodePath.nodes.at(i + 1);
                QString nodeName = nextNode->name()  + "-R";

                float deltaV = node->deltaV();

                if(nextNode->atmosphere())
                    deltaV = 0;

                course.names.insert(course.names.begin(), nodeName);
                course.types[nodeName] = nodePath.nodes.at(i + 1)->getType();
                course.deltaV[nodeName] = deltaV;
                course.totalDeltaV += deltaV;

                QString deltaVString = QVariant(deltaV).toString();

                if(deltaV == 0)
                    deltaVString = "Aerobrake";

                ui->listWidgetDeltaV->addItem(nodeName + " : " + deltaVString);
            }
        }

    }

    int nbStage = course.names.size();

    if(nbStage == 0)
        return course;

    float safeRatio = SAFE_RATIO;
    float decrement = (SAFE_RATIO - MINIMAL_RATIO) / ((course.totalDeltaV) / DV_MEAN_TRESHOLD);

    bool appendStage = false;
    QString nameToUse;
    float dvToAdd = 0;
    int currentPos = 0;

    for(int i = 0; i < nbStage; i++)
    {
        QString next = course.names[i];
        QString previous;

        if(i < nbStage - 1)
            previous = course.names[i + 1];
        else
            previous = next;

        float deltaV = course.deltaV[next];

        if(deltaV == 0 && i == nbStage - 1)
            continue;

        if(appendStage)
            next = nameToUse;

        deltaV = (deltaV + dvToAdd) * safeRatio;

        float futurDeltaV = deltaV + course.deltaV[previous] * (safeRatio - decrement);
        if((deltaV < DV_IGNORE_TRESHOLD || futurDeltaV < DV_MIN_TRESHOLD) &&
                course.types[previous] != PlanetNode::Body && course.types[next] != PlanetNode::Body)
        {
            if(deltaV == 0)
                continue;

            if(!appendStage)
            {
                nameToUse = next;
                appendStage = true;
            }
            dvToAdd += deltaV;
        }
        else
        {
            int division = deltaV / DV_MEAN_TRESHOLD;
            if(division < 1)
                division = 1;

            deltaV /= (float)division;

            for(int j = 0; j < division; j++)
            {
                QString appliedName;

                if(j > 0)
                {
                    appliedName = "(" + QVariant(currentPos).toString() + "." +
                            QVariant(j).toString() + ") " +
                            previous + " -> " + previous;
                }
                else
                {
                    int appliedPos = currentPos;

                    appliedName = "(" + QVariant(appliedPos).toString() + "." +
                            + "0) " + previous + " -> " + next;
                }

                course.stageNames.append(appliedName);
                course.stagesDeltaV.insert(appliedName, deltaV);
                course.totalStagesDeltaV += deltaV;

                if(appendStage)
                {
                    dvToAdd -= deltaV;

                    if(dvToAdd <= 0)
                        appendStage = false;

                    next = course.names[i];
                }

                safeRatio -= decrement;
                if(safeRatio < MINIMAL_RATIO)
                    safeRatio = MINIMAL_RATIO;
            }

            dvToAdd = 0;
            appendStage = false;
            currentPos++;
        }
    }
    ui->lineEditDVMin->setText(QVariant((int)course.totalDeltaV).toString());
    ui->lineEditDVSafe->setText(QVariant((int)course.totalStagesDeltaV).toString());
    ui->lineEditNbStages->setText(QVariant(course.stageNames.size()).toString());
}

void DestinationChooser::setCourse(const NodePath& nodePath)
{
    mCourse = createCourse(nodePath);
}
