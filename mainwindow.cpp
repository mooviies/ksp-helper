#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QMessageBox>
#include <QFile>
#include <QFileDialog>
#include <QErrorMessage>

#include "StageForm.h"
#include "DialogAbout.h"
#include "DialogLoadStage.h"

const quint32 FILE_HEADER = 0xAB00CC00;
const quint32 FILE_VERSION_100 = 0x100;
const quint32 FILE_VERSION = 0x101;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mSaved(true)
{
    ui->setupUi(this);
    mLastOpenPath = QDir::homePath() + "/Documents/KSPHelper";

    QDir baseDir(mLastOpenPath);
    if(!baseDir.exists())
        baseDir.mkdir(mLastOpenPath);

    ui->tableWidgetStages->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidgetStages->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->tableWidgetStages->horizontalHeader()->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setCourse(const Course& course)
{
    mCourse = course;

    updateCourseDisplay();
    updateStageDisplay();
}

void MainWindow::updateCourseDisplay(bool loading)
{
    ui->listWidgetDeltaV->clear();

    int minimal = mCourse.totalDeltaV;
    int safe = mCourse.totalStagesDeltaV;

    for(int i = 0; i < mCourse.names.size() - 1; i++)
    {
        const QString& name = mCourse.names.at(i);
        float deltaV = mCourse.deltaV[name];
        QString deltaVString = QVariant(deltaV).toString();

        if(deltaV == 0)
            deltaVString = "Aerobrake";

        ui->listWidgetDeltaV->insertItem(0, name + " : " + deltaVString);
    }

    ui->lineEditMinimal->setText(QVariant(minimal).toString());
    ui->lineEditSafe->setText(QVariant(safe).toString());

    if(mSaved && !loading)
    {
        mRocket.clear();
        for(int i = 0; i < mCourse.stageNames.size(); i++)
        {
            StageForm stageForm(mRocket, mCourse);
            mRocket.addStage(stageForm.getStage());
        }
        updateStageDisplay();
    }
}

void MainWindow::updateStageDisplay()
{
    ui->tableWidgetStages->clearContents();
    ui->tableWidgetStages->setRowCount(mRocket.size());

    for(int i = 0; i < mRocket.size(); i++)
    {
        const Stage* stage = mRocket.getStage(i);

        ui->tableWidgetStages->setItem(i, 0, new QTableWidgetItem(stage->name()));
        ui->tableWidgetStages->setItem(i, 1, new QTableWidgetItem(QVariant(stage->mass()).toString()));

        QTableWidgetItem* trustItem = new QTableWidgetItem(QVariant(stage->getTrustTotal().trustValue()).toString());
        ui->tableWidgetStages->setItem(i, 2, trustItem);

        QTableWidgetItem* deltaVItem = new QTableWidgetItem(QVariant(stage->deltaV()).toString());
        ui->tableWidgetStages->setItem(i, 3, deltaVItem);

        setColorState(trustItem,
                      stage->getTrustTotal().trustValue(),
                      stage->mass() * 10,
                      stage->mass() * 10 / MINIMAL_TRUST_RATIO,
                      "The trust can't lift the rocket",
                      "The trust is barely enough to lift the rocket");

        for(int j = 0; j <= 3; j++)
        {
            ui->tableWidgetStages->item(i, j)->setFlags(ui->tableWidgetStages->item(i, j)->flags() ^ Qt::ItemIsEditable);
            ui->tableWidgetStages->item(i, j)->setTextAlignment(Qt::AlignCenter);
        }
    }

    bool empty = ui->tableWidgetStages->rowCount() <= 0;
    ui->pushButtonRemove->setDisabled(empty);
    ui->tableWidgetStages->horizontalHeader()->setHidden(empty);

    ui->lineEditCurrent->setText(QVariant(mRocket.deltaV()).toString());
}

bool MainWindow::askForSave()
{
    if(!mSaved)
    {
        QMessageBox msgBox;
        msgBox.setText("The document has been modified.");
        msgBox.setInformativeText("Do you want to save your changes?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);

        int ret = msgBox.exec();

        switch (ret)
        {
           case QMessageBox::Save:
               on_actionSave_triggered();
               return true;
               break;
           case QMessageBox::Discard:
               return true;
               break;
           case QMessageBox::Cancel:
               return false;
               break;
           default:
               break;
         }
    }
    return true;
}

void MainWindow::save()
{
    if(saveTo(mPath, mCourse, mRocket) == FileState::OK)
        mSaved = true;
}

void MainWindow::load()
{
    if(loadFrom(mPath, mCourse, mRocket) == FileState::OK)
        mSaved = true;
}

FileState MainWindow::saveTo(const QString& path, const Course& course, const Rocket& rocket)
{
    QFile file(path);
    file.open(QIODevice::WriteOnly);

    if(file.isOpen())
    {
        QDataStream out(&file);

        out << FILE_HEADER;
        out << FILE_VERSION;

        out.setVersion(QDataStream::Qt_5_2);

        out << course << rocket;

        return FileState::OK;
    }
    else
        QMessageBox::critical(this, "Error while saving file", "The file could not be saved");

    return FileState::Failed;
}

FileState MainWindow::loadFrom(const QString& path, Course& course, Rocket& rocket)
{
    QFile file(path);
    file.open(QIODevice::ReadOnly);

    if(file.isOpen())
    {
        QDataStream in(&file);

        quint32 header, version;

        in >> header >> version;

        if(header != FILE_HEADER)
        {
            QMessageBox::critical(this, "Opening Error", "Wrong file format.");
            return FileState::BadFormat;
        }

        if(version < FILE_VERSION_100)
        {
            QMessageBox::critical(this, "Opening Error", "Deprecated file version.");
            return FileState::OldFormat;
        }
        else if(version > FILE_VERSION)
        {
            QMessageBox::critical(this, "Opening Error", "File version is too new. Update the software.");
            return FileState::NewFormat;
        }

        in.setVersion(QDataStream::Qt_5_2);

        if(version == FILE_VERSION_100)
        {
            Course_V100 course_V100;
            in >> course_V100 >> rocket;

            DestinationChooser destinationChooser;
            course = destinationChooser.getCourse(course_V100);
        }
        else
            in >> course >> rocket;

        updateCourseDisplay(true);
        updateStageDisplay();
        return FileState::OK;
    }
    else
        QMessageBox::critical(this, "Saving Error", "The file could not be open");

    return FileState::Failed;
}

void MainWindow::setFilePath(const QString& path)
{
    mPath = path;

    if(mPath.isEmpty())
        ui->actionSave->setEnabled(false);
    else
        ui->actionSave->setEnabled(true);
}

void MainWindow::on_pushButtonCreateCourse_clicked()
{
    hide();

    DestinationChooser* destination = new DestinationChooser(this);
    destination->exec();

    if(destination->Accepted)
        setCourse(destination->getCourse());

    show();
}

void MainWindow::on_tableWidgetStages_doubleClicked(const QModelIndex &index)
{
    hide();
    StageForm stageForm(mRocket, mCourse, mRocket.getStage(index.row()));
    int result = stageForm.exec();

    if(result == QDialog::Accepted)
    {
        mRocket.replaceStage(index.row(), stageForm.getStage());
        mSaved = false;
        updateStageDisplay();
    }
    show();
}

void MainWindow::on_actionSave_triggered()
{
    if(mPath.isEmpty())
        on_actionSaveAs_triggered();
    else
        save();
}

void MainWindow::on_actionSaveAs_triggered()
{
    QString path = QFileDialog::getSaveFileName(this, "Save As...",mLastOpenPath, "KSP Helper Files (*.khs)");

    if(!path.isEmpty())
    {
        mLastOpenPath = path;
        setFilePath(path);
        save();
    }
}

void MainWindow::on_actionOpen_triggered()
{
    QString path = QFileDialog::getOpenFileName(this, "Open...", mLastOpenPath, "KSP Helper Files (*.khs)");

    if(!path.isEmpty())
    {
        mLastOpenPath = path;
        setFilePath(path);
        load();
    }
}

void MainWindow::on_actionNew_triggered()
{
    if(askForSave())
    {
        mRocket.clear();
        mCourse.deltaV.clear();
        mCourse.names.clear();
        mCourse.totalDeltaV = 0;

        updateCourseDisplay();
        updateStageDisplay();

        setFilePath("");
        mSaved = true;
    }
}

void MainWindow::on_actionQuit_triggered()
{
    if(askForSave())
        close();
}

void MainWindow::on_action_About_triggered()
{
    DialogAbout about;
    about.exec();
}

void MainWindow::on_pushButtonNew_clicked()
{
    hide();
    StageForm stageForm(mRocket, mCourse);
    int result = stageForm.exec();

    if(result == QDialog::Accepted)
    {
        mRocket.addStage(stageForm.getStage());
        mSaved = false;
        updateStageDisplay();
    }
    show();
}

void MainWindow::on_pushButtonLoad_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, "Load Stage...", mLastOpenPath, "KSP Helper Files (*.khs)");

    if(!path.isEmpty())
    {
        mLastOpenPath = path;
        Rocket rocket;
        Course course;

        if(loadFrom(path, course, rocket) == FileState::OK)
        {
            DialogLoadStage loadStage(rocket);

            if(loadStage.exec() == QDialog::Accepted)
            {
                mRocket.addStage(loadStage.getStage());
                mSaved = false;
                updateStageDisplay();
            }
        }
    }
}

void MainWindow::on_pushButtonRemove_clicked()
{
    mRocket.removeStage();
    mSaved = false;
    updateStageDisplay();
}

void setColorState(QTableWidgetItem* item, float currentValue, float yellowValue, float greenValue,
                              const QString& redTooltip, const QString& yellowTooltip)
{
    if(currentValue < yellowValue)
    {
        item->setBackgroundColor(QColor(255, 204, 204));
        item->setToolTip(redTooltip);
    }
    else if(currentValue < greenValue)
    {
        item->setBackgroundColor(QColor(255, 255, 204));
        item->setToolTip(yellowTooltip);
    }
    else
    {
        item->setBackgroundColor(QColor(204, 255, 204));
        item->setToolTip("");
    }
}
