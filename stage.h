#ifndef STAGE_H
#define STAGE_H

#include <QString>
#include <QVector>
#include <QDataStream>

#include <cmath>

const float ISP_FACTOR = 9.82;
const float TOTAL_FUEL_RATIO = 1.125;
const float T_T_MIN_F_RATIO = 8.2;

/**
 * @brief The Mass class
 */
class Mass
{
public:
    Mass(){ set(0); }

    Mass(float value){ set(value); }

    void set(float value)
    {
        if(value >= 0)
            mValue = value;
        else
            mValue = 0;
    }

    float value() const { return mValue; }
private:
    float mValue;
};

/**
 * @brief The Fuel class
 */
class Fuel
{
public:
    Fuel(){ set(0, 0); }

    Fuel(float dryMass, float fuelMass){ set(dryMass, fuelMass); }

    Fuel(float totalMass)
    {
        if(totalMass < 0)
            totalMass = 0;

        float fuelMass = totalMass / TOTAL_FUEL_RATIO;

        switch((int)(totalMass * 10))
        {
        case 420:
            fuelMass = 32;
            break;
        case 820:
        case 410:
        case 205:
            fuelMass = floorf(totalMass - (totalMass / T_T_MIN_F_RATIO));
            break;
        default:
            break;
        }

        float dryMass = totalMass - fuelMass;

        set(dryMass, fuelMass);
    }

    void set(float dryMass, float fuelMass)
    {
        if(dryMass >= 0)
            mDryMass = dryMass;
        else
            mDryMass = 0;

        if(fuelMass >= 0)
            mFuelMass = fuelMass;
        else
            mFuelMass = 0;
    }

    float dryMass() const { return mDryMass; }
    float fuelMass() const { return mFuelMass; }
    float totalMass() const { return mDryMass + mFuelMass; }

private:
    float mDryMass;
    float mFuelMass;
};

/**
 * @brief The Trust class
 */
class Trust
{
public:
    Trust(){ set(0, 0); }

    Trust(float isp, float trust){ set(isp, trust); }

    void set(float isp, float trust)
    {
        if(isp >= 0)
            mISP = isp;
        else
            mISP = 0;

        if(trust >= 0)
            mTrust = trust;
        else
            mTrust = 0;
    }

    float isp() const { return mISP; }
    float trustValue() const { return mTrust; }

private:
    float mISP;
    float mTrust;
};

typedef QVector<Mass> MassList;
typedef QVector<Fuel> FuelList;
typedef QVector<Trust> TrustList;

/**
 * @brief The Stage class
 */
class Stage
{
public:
    Stage();
    Stage(const Stage& stage);
    ~Stage();

    Stage& operator=(const Stage& other);

    void addPart(Mass mass);
    void addFuel(Fuel fuel);
    void addTrust(Trust trust);

    void removePart(int id);
    void removeFuel(int id);
    void removeTrust(int id);

    void setParts(const MassList& parts);
    void setFuel(const FuelList& fuel);
    void setTrust(const TrustList& trust);

    void setNext(const Stage* next);
    void setName(const QString& name);

    const Stage* next() const;
    const QString& name() const;

    const MassList& getParts() const;
    const FuelList& getFuel() const;
    const TrustList& getTrust() const;

    Mass getPartTotal() const;
    Fuel getFuelTotal() const;
    Trust getTrustTotal() const;

    float mass() const;
    float dryMass() const;
    float deltaV() const;
    float massTrustRatio() const;

protected:
    void calculateMass();
    void calculateTrust();

private:
    const Stage* mNextStage;

    QString mName;

    MassList mParts;
    FuelList mFuel;
    TrustList mTrust;

    Mass mPartTotal;
    Fuel mFuelTotal;
    Trust mTrustTotal;
};

QDataStream& operator<<(QDataStream& out, const Mass& mass);
QDataStream& operator>>(QDataStream& in, Mass& mass);
QDataStream& operator<<(QDataStream& out, const Fuel& fuel);
QDataStream& operator>>(QDataStream& in, Fuel& fuel);
QDataStream& operator<<(QDataStream& out, const Trust& trust);
QDataStream& operator>>(QDataStream& in, Trust& trust);
QDataStream& operator<<(QDataStream& out, const Stage& stage);
QDataStream& operator>>(QDataStream& in, Stage& stage);

#endif // STAGE_H
