#-------------------------------------------------
#
# Project created by QtCreator 2014-04-10T17:54:57
#
#-------------------------------------------------

QT       += core gui script
QMAKE_CXXFLAGS += -std=c++0x

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KSPHelper
TEMPLATE = app

RC_FILE = base.rc

SOURCES += main.cpp\
    DestinationChooser.cpp \
    MainWindow.cpp \
    PlanetNode.cpp \
    PlanetSceneView.cpp \
    PlanetTree.cpp \
    Rocket.cpp \
    Stage.cpp \
    StageForm.cpp \
    DialogAbout.cpp \
    DialogLoadStage.cpp \
    LineEditToolTip.cpp

HEADERS  += \
    DestinationChooser.h \
    MainWindow.h \
    PlanetNode.h \
    PlanetSceneView.h \
    PlanetTree.h \
    Rocket.h \
    Stage.h \
    StageForm.h \
    DialogAbout.h \
    DialogLoadStage.h \
    LineEditToolTip.h

FORMS    += \
    DestinationChooser.ui \
    MainWindow.ui \
    StageForm.ui \
    DialogAbout.ui \
    DialogLoadStage.ui

RESOURCES += \
    base.qrc
