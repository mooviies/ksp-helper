#include "Rocket.h"

#include <exception>

Rocket::Rocket()
    : mDeltaV(0)
{
}

Rocket::~Rocket()
{
    clear();
}

void Rocket::addStage(const Stage& stage)
{
    Stage* last = NULL;

    if(mStages.size() > 0)
        last = mStages.last();

    mStages.append(new Stage(stage));
    mStages.last()->setNext(last);

    calculateDeltaV();
}

void Rocket::removeStage()
{
    if(mStages.size() > 0)
    {
        mDeltaV -= mStages.last()->deltaV();
        delete mStages.last();
        mStages.removeLast();
    }
}

void Rocket::clear()
{
    while(!mStages.empty())
        removeStage();

    mDeltaV = 0;
}

void Rocket::replaceStage(int i, const Stage& stage)
{
    const Stage* next = mStages[i]->next();

    *mStages[i] = stage;
    mStages[i]->setNext(next);

    calculateDeltaV();
}

const Stage* Rocket::getStage(int i) const
{
    if(i < mStages.size())
        return mStages.at(i);
    else
        throw std::out_of_range("The stages index is out of range!");
}

int Rocket::getStagePosition(const Stage* stage) const
{
    if(size() > 0)
        return mStages.indexOf((Stage*)stage);

    return 0;
}

void Rocket::setStages(const QVector<Stage>& stages)
{
    clear();

    for(int i = 0; i < stages.size(); i++)
    {
        addStage(stages.at(i));

        if(i > 0)
            mStages[i]->setNext(mStages[i - 1]);
    }
}

const QVector<Stage*>& Rocket::getAllStage() const
{
    return mStages;
}

int Rocket::size() const
{
    return mStages.size();
}

float Rocket::deltaV() const
{
    return mDeltaV;
}

void Rocket::calculateDeltaV()
{
    mDeltaV = 0;
    foreach(Stage* stage, mStages)
        mDeltaV += stage->deltaV();
}

QDataStream& operator<<(QDataStream& out, const Rocket& rocket)
{
    out << rocket.size();

    foreach(Stage* stage, rocket.getAllStage())
        out << *stage;

    return out;
}

QDataStream& operator>>(QDataStream& in, Rocket& rocket)
{
    int size = 0;
    in >> size;

    QVector<Stage> stages;
    for(int i = 0; i < size; i++)
    {
        Stage stage;
        in >> stage;
        stages.append(stage);
    }
    rocket.setStages(stages);

    return in;
}
