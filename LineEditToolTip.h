#ifndef LINEEDITTOOLTIP_H
#define LINEEDITTOOLTIP_H

#include <QLineEdit>

class LineEditToolTip : public QLineEdit
{
    Q_OBJECT
public:
    explicit LineEditToolTip(QWidget* parent = 0);

    void setToolTip(const QString& tooltip);

protected:
    void enterEvent(QEvent* event);

signals:

public slots:

private:
    QString mTooltip;

};

#endif // LINEEDITTOOLTIP_H
