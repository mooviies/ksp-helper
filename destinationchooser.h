#ifndef DESTINATIONCHOOSER_H
#define DESTINATIONCHOOSER_H

#include <QDialog>
#include <QGraphicsScene>

#include "PlanetTree.h"

const float MINIMAL_RATIO = 1.3;
const float SAFE_RATIO = 2.0;
const float MEAN_RATIO = (MINIMAL_RATIO + SAFE_RATIO) / 2.0;
const float DV_IGNORE_TRESHOLD = 1000;
const float DV_MIN_TRESHOLD = 2000;
const float DV_MAX_TRESHOLD = 3000;
const float DV_MEAN_TRESHOLD = (DV_MIN_TRESHOLD + DV_MAX_TRESHOLD) / 2.0;

namespace Ui {
class DestinationChooser;
}

/**
 * @brief Dialog to chose a celestial path.
 */
class DestinationChooser : public QDialog
{
    Q_OBJECT

public:
    explicit DestinationChooser(QWidget *parent = 0);
    ~DestinationChooser();

    /**
     * @brief Access the course
     * @return The selected celestial path
     */
    const Course& getCourse() const;
    const Course getCourse(const Course_V100& course_V100) const;

protected:
    virtual void showEvent(QShowEvent* event);
    virtual void resizeEvent(QResizeEvent* event);
    Course createCourse(const NodePath& nodePath) const;

private slots:
    void setCourse(const NodePath& nodePath);

private:
    Course mCourse;
    Ui::DestinationChooser *ui;
    QGraphicsScene mScene;
    QGraphicsPixmapItem* mBackgroundItem;

    QString mBackground;

    QString mDres;
    QString mDuna;
    QString mEeloo;
    QString mEve;
    QString mJool;
    QString mKerbin;
    QString mMoho;
    QString mLaythe;
    QString mKerbinBodies;

    QString mDunaLowOrbit;
    QString mEveLowOrbit;
    QString mJoolLowOrbit;
    QString mKerbinLowOrbit;
    QString mKerbinIntercept;

    PlanetTree mPlanetTree;
};

#endif // DESTINATIONCHOOSER_H
