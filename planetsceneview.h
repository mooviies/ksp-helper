#ifndef PLANETSCENEVIEW_H
#define PLANETSCENEVIEW_H

#include <QGraphicsView>

#include "PlanetTree.h"

/**
 * @brief The PlanetSceneView class
 */
class PlanetSceneView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit PlanetSceneView(QWidget *parent = 0);

    void setBackground(QGraphicsItem* background);
    void setPlanetTree(PlanetTree* planetTree);

signals:
    void courseChanged(const NodePath& course);
    void courseSelected();

public slots:

protected:
    virtual void mouseMoveEvent(QMouseEvent* event);
    virtual void mouseReleaseEvent(QMouseEvent* event);

private:
    float mScale;
    NodePath mLastCourse;
    NodePath mSavedCourse;
    QGraphicsItem* mLastSelectedItem;
    QGraphicsItem* mBackground;
    PlanetTree* mPlanetTree;
};

#endif // PLANETSCENEVIEW_H
