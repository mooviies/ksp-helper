#include "PlanetNode.h"

PlanetNode::PlanetNode(QString name, QString imagePath, QPointF imagePos, QGraphicsScene& scene, NodeType type, bool atmosphere)
    : mParent(NULL)
    , mParentDistance(0)
    , mAtmosphere(atmosphere)
    , mType(type)
    , mName(name)
    , mItem(scene.addPixmap(QPixmap(imagePath)))
{
    mItem->setTransformationMode(Qt::SmoothTransformation);
    mItem->setTransformOriginPoint(mItem->boundingRect().center());
    mItem->setPos(imagePos - mItem->boundingRect().center());
}

QGraphicsPixmapItem* PlanetNode::getItem()
{
    return mItem;
}

const QGraphicsPixmapItem* PlanetNode::getItem() const
{
    return mItem;
}

QDataStream& operator<<(QDataStream& out, PlanetNode::NodeType& nodeType)
{
    out << nodeType;
    return out;
}

QDataStream& operator>>(QDataStream& in, PlanetNode::NodeType& nodeType)
{
    in >> nodeType;
    return in;
}
