#include "Stage.h"

#include <cmath>

using namespace std;

Stage::Stage()
    : mNextStage(0)
    , mPartTotal(0)
    , mFuelTotal(0)
    , mTrustTotal(0, 0)
{
    calculateMass();
    calculateTrust();
}

Stage::Stage(const Stage& stage)
{
    *this = stage;
}

Stage::~Stage()
{
}

Stage& Stage::operator=(const Stage& other)
{
    this->mFuel = other.mFuel;
    this->mFuelTotal = other.mFuelTotal;
    this->mName = other.mName;
    this->mNextStage = other.mNextStage;
    this->mParts = other.mParts;
    this->mPartTotal = other.mPartTotal;
    this->mTrust = other.mTrust;
    this->mTrustTotal = other.mTrustTotal;
    return *this;
}

void Stage::addPart(Mass mass)
{
    mParts.append(mass);
    calculateMass();
}

void Stage::addFuel(Fuel fuel)
{
    mFuel.append(fuel);
    calculateMass();
}

void Stage::addTrust(Trust trust)
{
    mTrust.append(trust);
    calculateTrust();
}

void Stage::removePart(int id)
{
    if(id < mParts.size())
    {
        mParts.remove(id);
        calculateMass();
    }
}

void Stage::removeFuel(int id)
{
    if(id < mFuel.size())
    {
        mFuel.remove(id);
        calculateMass();
    }
}

void Stage::removeTrust(int id)
{
    if(id < mTrust.size())
    {
        mTrust.remove(id);
        calculateTrust();
    }
}

void Stage::setParts(const MassList& parts)
{
    mParts = parts;
    calculateMass();
}

void Stage::setFuel(const FuelList& fuel)
{
    mFuel = fuel;
    calculateMass();
}

void Stage::setTrust(const TrustList& trust)
{
    mTrust = trust;
    calculateTrust();
}

void Stage::setNext(const Stage* next)
{
    mNextStage = next;
}

void Stage::setName(const QString& name)
{
    mName = name;
}

const Stage* Stage::next() const
{
    return mNextStage;
}

const QString& Stage::name() const
{
    return mName;
}

const MassList& Stage::getParts() const
{
    return mParts;
}

const FuelList& Stage::getFuel() const
{
    return mFuel;
}

const TrustList& Stage::getTrust() const
{
    return mTrust;
}

Mass Stage::getPartTotal() const
{
    return mPartTotal;
}

Fuel Stage::getFuelTotal() const
{
    return mFuelTotal;
}

Trust Stage::getTrustTotal() const
{
    return mTrustTotal;
}

float Stage::mass() const
{
    float mass = getPartTotal().value() + getFuelTotal().totalMass();

    if(mNextStage)
        mass += mNextStage->mass();

    return mass;
}

float Stage::dryMass() const
{
    float mass = getPartTotal().value() + getFuelTotal().dryMass();

    if(mNextStage)
        mass += mNextStage->mass();

    return mass;
}

float Stage::deltaV() const
{
    if(dryMass() == 0)
        return 0;

    return getTrustTotal().isp() * log(mass() / dryMass()) * ISP_FACTOR;
}

float Stage::massTrustRatio() const
{
    if(getTrustTotal().trustValue() == 0)
        return 0;

    return mass() / getTrustTotal().trustValue();
}

void Stage::calculateMass()
{
    float partTotal = 0, fuelTotal = 0, dryFuelTotal = 0;

    foreach(Mass mass, mParts)
        partTotal += mass.value();

    foreach(Fuel fuel, mFuel)
    {
        fuelTotal += fuel.fuelMass();
        dryFuelTotal += fuel.dryMass();
    }

    mPartTotal.set(partTotal);
    mFuelTotal.set(dryFuelTotal, fuelTotal);
}

void Stage::calculateTrust()
{
    float ispTotal = 0, trustTotal = 0;

    foreach(Trust trust, mTrust)
    {
        if(trust.isp() != 0)
        {
            ispTotal += trust.trustValue() / trust.isp();
            trustTotal += trust.trustValue();
        }
    }

    if(ispTotal == 0)
        mTrustTotal.set(0, 0);

    ispTotal = trustTotal / ispTotal;

    mTrustTotal.set(ispTotal, trustTotal);
}

QDataStream& operator<<(QDataStream& out, const Mass& mass)
{
    out << mass.value();
    return out;
}

QDataStream& operator>>(QDataStream& in, Mass& mass)
{
    float value = 0;

    in >> value;
    mass.set(value);

    return in;
}

QDataStream& operator<<(QDataStream& out, const Fuel& fuel)
{
    out << fuel.dryMass() << fuel.fuelMass();
    return out;
}

QDataStream& operator>>(QDataStream& in, Fuel& fuel)
{
    float dryMass = 0, fuelMass = 0;

    in >> dryMass >> fuelMass;
    fuel.set(dryMass, fuelMass);

    return in;
}

QDataStream& operator<<(QDataStream& out, const Trust& trust)
{
    out << trust.isp() << trust.trustValue();
    return out;
}

QDataStream& operator>>(QDataStream& in, Trust& trust)
{
    float isp = 0, trustValue = 0;

    in >> isp >> trustValue;
    trust.set(isp, trustValue);

    return in;
}

QDataStream& operator<<(QDataStream& out, const Stage& stage)
{
    out << stage.name() << stage.getParts()
        << stage.getFuel() << stage.getTrust();

    return out;
}

QDataStream& operator>>(QDataStream& in, Stage& stage)
{
    QString name;
    QVector<Mass> parts;
    QVector<Fuel> fuel;
    QVector<Trust> trust;

    in >> name >> parts >> fuel >> trust;

    stage.setName(name);
    stage.setParts(parts);
    stage.setFuel(fuel);
    stage.setTrust(trust);

    return in;
}
