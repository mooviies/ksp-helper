#ifndef STAGEFORM_H
#define STAGEFORM_H

#include <QDialog>

#include "Rocket.h"
#include "PlanetTree.h"

const float MINIMAL_TRUST_RATIO = 0.75;

namespace Ui {
class StageForm;
}

/**
 * @brief The StageForm class
 */
class StageForm : public QDialog
{
    Q_OBJECT

public:
    explicit StageForm(const Rocket& rocket, const Course& course, QWidget *parent = 0);
    explicit StageForm(const Rocket& rocket, const Course& course, const Stage* stage, QWidget *parent = 0);
    ~StageForm();

    const Stage& getStage() const;

public slots:
    void deleteItem();
    void deleteItemPart();
    void deleteItemFuel();
    void deleteItemTrust();

protected:
    void processParts();
    void processFuel();
    void processTrust();
    void processStageValues();

    void calculateFuel();

    void displayNextStage();
    void calculateRecommendedV();
    void displayRecommendedV(const QString& name, float recommended);

    void configureShortcut();

private slots:
    void on_buttonAddPart_clicked();
    void on_buttonAddFuel_clicked();
    void on_buttonAddTrust_clicked();

    void on_lineEditPart_textChanged(const QString &arg1);
    void on_lineEditContainerFuel_textChanged(const QString &arg1);
    void on_lineEditFuel_textChanged(const QString &arg1);
    void on_lineEditISP_textChanged(const QString &arg1);
    void on_lineEditTrust_textChanged(const QString &arg1);

    void on_checkBoxAutomatic_toggled(bool checked);

    void on_listWidgetDry_itemSelectionChanged();
    void on_listWidgetFuel_itemSelectionChanged();
    void on_listWidgetISP_itemSelectionChanged();
    void on_listWidgetTrust_itemSelectionChanged();

    void on_lineEditName_textChanged(const QString &arg1);

private:
    Ui::StageForm *ui;
    Stage mStage;
    int mStagePos;

    const Rocket& mRocket;
    const Course& mCourse;
    QPalette mLineEditPalette;

    float mRecommended;
};

void setColorState(QWidget* widget, float currentValue, float yellowValue, float greenValue, const QString& redTooltip = "", const QString& yellowTooltip = "");

#endif // STAGEFORM_H
