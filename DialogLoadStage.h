#ifndef DIALOGLOADSTAGE_H
#define DIALOGLOADSTAGE_H

#include <QDialog>

#include "Rocket.h"

namespace Ui {
class DialogLoadStage;
}

class DialogLoadStage : public QDialog
{
    Q_OBJECT

public:
    explicit DialogLoadStage(const Rocket& rocket, QWidget *parent = 0);
    ~DialogLoadStage();

    const Stage& getStage() const;

private slots:
    void on_tableWidgetStages_doubleClicked(const QModelIndex &index);
    void on_tableWidgetStages_itemSelectionChanged();

private:
    Ui::DialogLoadStage *ui;
    QVector<Stage> mStages;
    int mSelection;
};

#endif // DIALOGLOADSTAGE_H
