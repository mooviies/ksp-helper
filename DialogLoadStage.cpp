#include "DialogLoadStage.h"
#include "ui_DialogLoadStage.h"

DialogLoadStage::DialogLoadStage(const Rocket& rocket, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogLoadStage),
    mSelection(0)
{
    ui->setupUi(this);

    const QVector<Stage*>& stages = rocket.getAllStage();

    for(int i = 0; i < stages.size(); i++)
    {
        mStages.append(*stages.at(i));
        ui->tableWidgetStages->insertRow(i);
        ui->tableWidgetStages->setItem(i, 0, new QTableWidgetItem(stages.at(i)->name()));
        ui->tableWidgetStages->setItem(i, 1, new QTableWidgetItem(QVariant(stages.at(i)->mass()).toString()));
        ui->tableWidgetStages->setItem(i, 2, new QTableWidgetItem(QVariant(stages.at(i)->deltaV()).toString()));
    }
}

DialogLoadStage::~DialogLoadStage()
{
    delete ui;
}

const Stage& DialogLoadStage::getStage() const
{
    return mStages.at(mSelection);
}

void DialogLoadStage::on_tableWidgetStages_doubleClicked(const QModelIndex &/*index*/)
{
    accept();
}

void DialogLoadStage::on_tableWidgetStages_itemSelectionChanged()
{
    bool empty = ui->tableWidgetStages->selectedItems().empty();
    ui->pushButtonOK->setDisabled(empty);
    mSelection = ui->tableWidgetStages->row(ui->tableWidgetStages->selectedItems().at(0));
}
