#include "PlanetTree.h"

PlanetTree::PlanetTree(PlanetNode* root)
{
    setRoot(root);
}

PlanetTree::~PlanetTree()
{
    foreach(PlanetNode* node, mAllNode)
        delete node;
}

PlanetNode* PlanetTree::setRoot(PlanetNode* root)
{
    mRoot = root;

    if(mRoot)
        mAllNode.insert(mRoot->getItem(), mRoot);

    return mRoot;
}

PlanetNode* PlanetTree::addNode(PlanetNode* parent, PlanetNode* node, float distance)
{
    if(mRoot)
    {
        if(mAllNode.contains(parent->getItem()) && !mAllNode.contains(node->getItem()))
        {
            node->mParent = parent;
            node->mParentDistance = distance;
            parent->mChilds.append(node);
            parent->mChildsDistance.append(distance);
            mAllNode.insert(node->getItem(), node);
            mNodeByName.insert(node->name(), node);
            return node;
        }
    }
    return 0;
}

const PlanetNode* PlanetTree::getNode(QGraphicsItem* item) const
{
    if(mAllNode.contains(item))
        return mAllNode.value(item);
    else
        return 0;
}

const PlanetNode* PlanetTree::getNode(const QString& name) const
{
    return mNodeByName.value(name);
}

NodePath PlanetTree::getPath(const PlanetNode* destination) const
{
    NodePath path;

    if(mAllNode.contains((QGraphicsItem*)destination->getItem()))
        getRecursiveCourse(destination, path);

    return path;
}

void PlanetTree::getRecursiveCourse(const PlanetNode* currentNode, NodePath& course) const
{
    course.nodes.append(currentNode);

    if(currentNode->mParent)
    {
        course.totalDistance += currentNode->mParentDistance;
        getRecursiveCourse(currentNode->mParent, course);
    }
}

QDataStream& operator<<(QDataStream& out, const Course_V100& course)
{
    out << course.names << course.deltaV;
    return out;
}

QDataStream& operator>>(QDataStream& in, Course_V100& course)
{
    QVector<QString> names;
    QHash<QString, float> deltaV;

    float totalDeltaV = 0;

    in >> names >> deltaV;

    foreach(float v, deltaV)
        totalDeltaV += v;

    course.names = names;
    course.deltaV = deltaV;
    course.totalDeltaV = totalDeltaV;

    return in;
}

QDataStream& operator<<(QDataStream& out, const Course& course)
{
    out << course.names << course.deltaV << course.stageNames << course.stagesDeltaV << course.types.size();

    foreach(QString name, course.types.keys())
    {
        out << name << (int)course.types.value(name);
    }

    return out;
}

QDataStream& operator>>(QDataStream& in, Course& course)
{
    QVector<QString> names;
    QHash<QString, float> deltaV;
    QVector<QString> stageNames;
    QHash<QString, float> stagesDeltaV;
    QHash<QString, PlanetNode::NodeType> types;
    int typeSize = 0;

    float totalDeltaV = 0;
    float totalStagesDeltaV = 0;

    in >> names >> deltaV >> stageNames >> stagesDeltaV >> typeSize;

    for(int i = 0; i < typeSize; i++)
    {
        QString name;
        int nodeType = 0;
        in >> name >> nodeType;
        types.insert(name, (PlanetNode::NodeType)nodeType);
    }

    foreach(float v, deltaV)
        totalDeltaV += v;

    foreach(float v, stagesDeltaV)
        totalStagesDeltaV += v;

    course.names = names;
    course.deltaV = deltaV;
    course.totalDeltaV = totalDeltaV;

    course.stageNames = stageNames;
    course.stagesDeltaV = stagesDeltaV;
    course.totalStagesDeltaV = totalStagesDeltaV;
    course.types = types;

    return in;
}
