#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>

#include "Rocket.h"
#include "DestinationChooser.h"

namespace Ui {
class MainWindow;
}

enum FileState
{
    OK,
    BadFormat,
    OldFormat,
    NewFormat,
    Failed
};

/**
 * @brief The MainWindow class
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setFilePath(const QString& path);
    void load();

protected:
    void setCourse(const Course& course);

    void updateCourseDisplay(bool loading = false);
    void updateStageDisplay();

    bool askForSave();
    void save();

    FileState saveTo(const QString& path, const Course& course, const Rocket& rocket);
    FileState loadFrom(const QString& path, Course& course, Rocket& rocket);

private slots:
    void on_pushButtonCreateCourse_clicked();
    void on_tableWidgetStages_doubleClicked(const QModelIndex &index);

    void on_actionSave_triggered();
    void on_actionSaveAs_triggered();
    void on_actionOpen_triggered();
    void on_actionNew_triggered();
    void on_actionQuit_triggered();
    void on_action_About_triggered();

    void on_pushButtonNew_clicked();
    void on_pushButtonLoad_clicked();
    void on_pushButtonRemove_clicked();

private:
    Ui::MainWindow *ui;
    Rocket mRocket;
    Course mCourse;

    QString mLastOpenPath;
    QString mPath;
    bool mSaved;
};

void setColorState(QTableWidgetItem* item, float currentValue, float yellowValue, float greenValue,
                              const QString& redTooltip = "", const QString& yellowTooltip = "");

#endif // MAINWINDOW_H
