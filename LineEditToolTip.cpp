#include "LineEditToolTip.h"

#include <QToolTip>

LineEditToolTip::LineEditToolTip(QWidget *parent) :
    QLineEdit(parent)
{
    mTooltip = toolTip();
    setMouseTracking(true);

    if(!toolTip().isEmpty())
        setToolTip("");
}

void LineEditToolTip::setToolTip(const QString& tooltip)
{
    mTooltip = tooltip;
}

void LineEditToolTip::enterEvent(QEvent* /*event*/)
{
    if(!mTooltip.isEmpty())
        QToolTip::showText(mapToGlobal(QPoint()), mTooltip);
}
